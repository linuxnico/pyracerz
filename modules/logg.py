#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

from datetime import datetime
from loguru import logger
from sys import stdout
NIVEAULOG = 2
logger.remove(None)
logger.add("log/log.log",
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>',
           rotation="1 MB",
           level=NIVEAULOG)
logger.add(stdout,
           format='<green>{time:DD-MM-YYYY HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level><white>{message}</white></level>',
           level=NIVEAULOG)
date_Heure = str(datetime.now().day).zfill(2) + "-" + str(datetime.now().month).zfill(2) + "-" + str(datetime.now().year) + "-" + str(datetime.now().hour) + "-" + str(datetime.now().minute) + "-" + str(datetime.now().second)
heure_debut = 'Debut du script... le ' + date_Heure
logger.info(heure_debut)
