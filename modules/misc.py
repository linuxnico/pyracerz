# Copyright (C) 2005    Jujucece <jujucece@gmail.com>
#
# This file is part of pyRacerz.
#
# pyRacerz is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyRacerz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyRacerz; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA    02110-1301    USA

import pygame
from pygame.locals import QUIT, KEYDOWN

import random
import os
import sys
import configparser as ConfigParser
import hashlib
from logg import logger
logger.debug('module misc uploaded')

VERSION = "0.2.1"

lightColor = (230, 230, 255)
darkColor = (118, 118, 151)

background = None
screen = None

popUpFont = None
titleFont = None
itemFont = None
smallItemFont = None
bigFont = None
fullscreen = False
music = False
configCar = False
doubleBuf = True
zoom = 1
#  slide lateral indice
slide = 0.2
if os.path.exists('.pyRacerz.conf'):
    with open('.pyRacerz.conf', 'r') as fread:
        confFile = ConfigParser.ConfigParser()
        confFile.read(fread)
        if confFile.has_section('options'):
            logger.debug('option exists')
            if "fullscreen" in confFile['options']:
                logger.debug('fullscreen exists')
                fullscreen = confFile.get('options', 'fullscreen') == '1'
            if "music" in confFile['options']:
                logger.debug('music exists')
                music = confFile.get('options', 'music') == '1'
            if "doubleBuf" in confFile['options']:
                logger.debug('doublebuf exists')
                doubleBuf = confFile.get('options', 'doublebuf') == '1'
            if "zoom" in confFile['options']:
                logger.debug('zoom exists')
                zoom = float(confFile.get('options', 'zoom'))
        logger.debug('fs:{} - mus:{} - zoom:{} - doublebuf:{}'.format(fullscreen, music, zoom, doubleBuf))


def init():
    """Init."""
    global popUpFont
    global titleFont
    global itemFont
    global smallItemFont
    global bigFont
    global background

    try:
        popUpFont = pygame.font.Font(os.path.join("fonts", "arcade_pizzadude", "ARCADE.TTF"), int(24 * zoom))
        titleFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(52 * zoom))
        itemFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(34 * zoom))
        smallItemFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(30 * zoom))
        bigFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(66 * zoom))
    except Exception as e:
        print("Cannot initialize fonts:")
        print(e)
        sys.exit(-1)

    background = pygame.transform.scale(pygame.image.load(os.path.join("sprites", "background.png")).convert(), (int(1024 * zoom), int(768 * zoom)))


def chrono2Str(chrono):
    """Format chrono to string."""
    return str(chrono / 100.0).replace(".", "''")


def wait4Key():
    """Wait a key pressed."""
    # Clear event queue
    pygame.event.clear()

    # Wait for key Input
    ok = 0
    while ok == 0:
        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit(0)
            if event.type == KEYDOWN:
                ok = 1
                break

    # Clear event queue
    pygame.event.clear()


def startRandomMusic():
    """Start random music in music forlder."""
    global music

    stopMusic()

    if music == 1:
        # Randomly choose the Music among .ogg files
        musics = []
        listFiles = os.listdir("musics")
        for fileMusic in listFiles:
            if fileMusic.endswith(".ogg") or fileMusic.endswith(".OGG"):
                musics.append(fileMusic)

        if len(musics) > 0:
            rand = random.randint(0, len(musics) - 1)
            try:
                pygame.mixer.music.load(os.path.join("musics", musics[rand]))
                pygame.mixer.music.play()
            except Exception as e:
                print("Music: {} unable to play...".format(musics[rand]))
                print(e)


def stopMusic():
    """Stop Music."""
    pygame.mixer.music.fadeout(1000)


class PopUp:
    """Class pop up."""

    def __init__(self, track):
        self.track = track
        self.listElement = []
        self.rect = pygame.Rect(0, 688 * zoom, 260 * zoom, 80 * zoom)

    def addElement(self, car, text):
        self.listElement.append([car, text, 0])

    def display(self):

        # Erase PopUp Area
        screen.blit(self.track.track, self.rect, self.rect)

        # If useful, display PopUp Area
        if self.listElement != []:

            y = 750 * zoom

            for elem in self.listElement:
                x = 0
                carMini = elem[0].miniCar
                carMiniRect = carMini.get_rect()
                carMiniRect.x = x
                x = x + carMiniRect.width

                text = popUpFont.render(elem[1], 1, lightColor, (0, 0, 0))
                textRect = text.get_rect()
                textRect.x = x
                textRect.y = y
                screen.blit(text, textRect)

                carMiniRect.centery = textRect.centery
                screen.blit(carMini, carMiniRect)

                # Remove an old element
                if elem[2] == 400:
                    self.listElement.remove(elem)
                else:
                    elem[2] = elem[2] + 1
                y = y - textRect.height


def addHiScore(track, player):
    """Add hiscore to file."""
    fileExist = 1

    confFile = ConfigParser.ConfigParser()
    try:
        confFile.read(".pyRacerz.conf")
    except Exception:
        fileExist = 0

    # If the track is not represented, create it
    if fileExist == 0 or not confFile.has_section("hi {}".format(track.name)):
        fwrite = open(".pyRacerz.conf", "w+")
        confFile.add_section("hi " + track.name)
        confFile.write(fwrite)
        confFile.read(".pyRacerz.conf")

    # For the Inverse
    if track.reverse == 0:
        level = player.level
    else:
        level = -player.level

    # If the Level is not represented create it and put the Hi-scores
    if not confFile.has_option("hi " + track.name, "level" + str(level)):
        h = hashlib.sha1(str(track.name).encode())
        h.update(str("level" + str(level)).encode())
        h.update(player.name.encode())
        h.update(str(player.bestChrono).encode())
        fwrite = open(".pyRacerz.conf", "w+")
        confFile.set("hi {}".format(track.name), "level{}".format(level), "{} {} {}".format(player.name, player.bestChrono, h.hexdigest()))
        confFile.write(fwrite)
        return 1
    else:
        hi = confFile.get("hi {}".format(track.name), "level{}".format(str(level).split()[0]))
        h = hashlib.sha1(str(track.name).encode())
        h.update(str("level" + str(level)).encode())
        h.update(hi[0].encode())
        h.update(hi[1].encode())
        if hi[2] == h.hexdigest():
            if int(hi[1]) > player.bestChrono:
                h = hashlib.sha1(str(track.name).encode())
                h.update(str("level" + str(level)).encode())
                h.update(player.name.encode())
                h.update(str(player.bestChrono).encode())
                fwrite = open(".pyRacerz.conf", "w+")
                confFile.set("hi {}".format(track.name), "level{}".format(level), "{} {} {}".format(player.name, player.bestChrono, h.hexdigest()))
                confFile.write(fwrite)
                return 1
            else:
                return 0
        else:
            # If the HiScore is Corrupted, erase it
            h = hashlib.sha1(str(track.name).encode())
            h.update(str("level" + str(level)).encode())
            h.update(player.name.encode())
            h.update(str(player.bestChrono).encode())
            fwrite = open(".pyRacerz.conf", "w+")
            confFile.set("hi {}".format(track.name), "level{}".format(level), "{} {} {}".format(player.name, player.bestChrono, h.hexdigest()))
            confFile.write(fwrite)
            return 1


def getUnlockLevel():
    """Get unlock level."""
    confFile = ConfigParser.ConfigParser()
    try:
        confFile.read(".pyRacerz.conf")
    except Exception:
        return 0

    if not confFile.has_section("unlockLevel"):
        return 0
    if not confFile.has_option("unlockLevel", "key"):
        return 0

    key = confFile.get("unlockLevel", "key").split()
    h = hashlib.sha1(b"pyRacerz")
    h.update(str(key[0]).encode())
    if h.hexdigest() == key[1]:
        return key[0]
    else:
        return 0


def setUnlockLevel(lck):
    """Set unlock level."""
    # Only change the unlock level if it's better than the actual one
    if getUnlockLevel() >= lck:
        return

    fileExist = 1

    confFile = ConfigParser.ConfigParser()
    try:
        confFile.read(".pyRacerz.conf")
    except Exception:
        fileExist = 0

    if fileExist == 0 or not confFile.has_section("unlockLevel"):
        fwrite = open(".pyRacerz.conf", "w+")
        confFile.add_section("unlockLevel")
        confFile.write(fwrite)
        confFile.read(".pyRacerz.conf")

    h = hashlib.sha1(b"pyRacerz")
    h.update(str(lck).encode())
    fwrite = open(".pyRacerz.conf", "w+")
    confFile.set("unlockLevel", "key", str(lck) + " " + h.hexdigest())
    confFile.write(fwrite)
