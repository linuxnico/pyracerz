# Copyright (C) 2005    Jujucece <jujucece@gmail.com>
#
# This file is part of pyRacerz.
#
# pyRacerz is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# pyRacerz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyRacerz; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA    02110-1301    USA

import pygame
# from pygame.locals import *

import os
import configparser as ConfigParser
import zlib

import misc

from logg import logger
logger.debug('module track uploaded')


def getImageFromTrackName(name):
    """Get image from track name, like thumbnail."""
    # If it's a bonus (locked) track, act differently
    if type(name) is bytes:
        name = name.decode('utf-8')
    if name.startswith("bonus"):
        return pygame.image.fromstring(zlib.decompress(open(os.path.join("tracks", "{}.png".format(name)), "rb").read()), (1024, 768), "RGBA").convert()

    return pygame.image.load(os.path.join("tracks", "{}.png".format(name))).convert()


def getImageFFromTrackName(name):
    """Get forward image from track name, like thumbnail."""
    # If it's a bonus (locked) track, act differently
    if type(name) is bytes:
        name = name.decode('utf-8')
    if name.startswith("bonus"):
        return pygame.image.fromstring(zlib.decompress(open(os.path.join("tracks", "{}F.png".format(name)), "rb").read()), (1024, 768), "RGBA").convert()

    return pygame.image.load(os.path.join("tracks", "{}F.png".format(name))).convert()


class Track:
    """Class representing a track (with the 2 track pictures)."""

    def __init__(self, name, reverse=0):
        self.track = pygame.transform.scale(getImageFromTrackName(name), (int(1024 * misc.zoom), int(768 * misc.zoom)))
        self.trackF = pygame.transform.scale(getImageFFromTrackName(name), (int(1024 * misc.zoom), int(768 * misc.zoom)))
        confFile = ConfigParser.ConfigParser()
        if type(name) is bytes:
            name = name.decode('utf-8')
        confFile.read(os.path.join("tracks", "{}.conf".format(name)))
        self.name = name
        self.author = confFile.get("track", "author")
        self.nbCheckpoint = int(confFile.get("track", "nbCheckpoint"))

        # Flag use to race in the opposite way
        self.reverse = reverse

        if self.reverse == 0:
            section = "normal"
        else:
            section = "reverse"

        self.startX1 = int(confFile.get(section, "startX1")) * misc.zoom
        self.startY1 = int(confFile.get(section, "startY1")) * misc.zoom
        self.startX2 = int(confFile.get(section, "startX2")) * misc.zoom
        self.startY2 = int(confFile.get(section, "startY2")) * misc.zoom
        self.startX3 = int(confFile.get(section, "startX3")) * misc.zoom
        self.startY3 = int(confFile.get(section, "startY3")) * misc.zoom

        self.startAngle = float(confFile.get(section, "startAngle"))


def getAvailableTrackNames():
    """Find tracks with browsing and finding the 3 files."""
    listAvailableTrackNames = []

    listFiles = os.listdir("tracks")
    for fileTrack in listFiles:
        if fileTrack.endswith(".conf"):
            trackName = fileTrack.replace(".conf", "")
            track = 1

            # Test if the user has unlocked the Bonus Level
            try:
                if type(trackName) is bytes:
                    trackName = trackName.decode('utf-8')
                if trackName.startswith("bonus") and misc.getUnlockLevel() < int(trackName.replace("bonus", "")):
                    continue
            except Exception:
                continue

            for fileTrack2 in listFiles:
                if fileTrack2 == trackName + ".png":
                    track = track + 1
                if fileTrack2 == trackName + "F.png":
                    track = track + 1
            if track == 3:
                listAvailableTrackNames.append(trackName)
    return listAvailableTrackNames
