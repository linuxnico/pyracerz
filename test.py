#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = '1'

import time
import sys
sys.path.append("modules")
import misc
from logg import logger
import game
import menu
import track
import replay
import challenge
import pygame
import os

if __name__ == '__main__':

    pygame.init()
    misc.screen = pygame.display.set_mode((int(1024 * misc.zoom), int(768 * misc.zoom)), misc.HWSURFACE, 24)

    misc.popUpFont = pygame.font.Font(os.path.join("fonts", "arcade_pizzadude", "ARCADE.TTF"), int(24 * misc.zoom))
    misc.titleFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(52 * misc.zoom))
    misc.itemFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(34 * misc.zoom))
    misc.smallItemFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(30 * misc.zoom))
    misc.bigFont = pygame.font.Font(os.path.join("fonts", "alba", "ALBA____.TTF"), int(66 * misc.zoom))
    # misc.init()
    # # test menu value
    # menutest = menu.ChooseValueMenu(misc.titleFont, "singleRace: chooseNbLaps", 0, misc.itemFont, 1, 10)
    # print(menutest.getInput())
    #
    # # test menu text value
    # menutest = menu.ChooseTextMenu(misc.titleFont, "singleRace: chooseNbLaps", 0, misc.itemFont, 5)
    # print(menutest.getInput())

    # # test menu Player
    # i = 1
    # menutest = menu.ChooseHumanPlayerMenu(misc.titleFont, "singleRace: chooseHumanPlayer" + str(i), 5 * misc.zoom, misc.itemFont)
    # print(menutest.getInput())

    # test menu Player
    # i = 1
    # menutest = menu.ChooseOptionsMenu(misc.titleFont, "singleRace: chooseHumanPlayer" + str(i), 5 * misc.zoom, misc.itemFont)
    # print(menutest.getInput())

    # soundStart = [pygame.mixer.Sound(os.path.join("sounds", 'start_1.ogg')),
    #               pygame.mixer.Sound(os.path.join("sounds", 'start_2.ogg')),
    #               pygame.mixer.Sound(os.path.join("sounds", 'start_3.ogg')),
    #               pygame.mixer.Sound(os.path.join("sounds", 'start_4.ogg'))]
    # pygame.mixer.Sound.play(soundStart[1])
    # time.sleep(2)
    carFileBreak = os.path.join("sprites", "cars", "car{}B.png".format(0))
    image = pygame.image.load(carFileBreak).convert_alpha()
    print(image.get_rect().size)
